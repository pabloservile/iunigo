package com.iunigo.university.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

@JsonRootName(value = "status")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StatusApi {

    @JsonProperty("code")
    private String code;

    @JsonProperty("message")
    private String message;

    public StatusApi(String code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

}
