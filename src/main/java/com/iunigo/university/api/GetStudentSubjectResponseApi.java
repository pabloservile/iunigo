package com.iunigo.university.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.iunigo.university.domain.model.Subject;
import jdk.net.SocketFlow;
import lombok.*;

import java.util.List;

@JsonRootName(value = "getStudentSubjectResponseApi")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class GetStudentSubjectResponseApi extends StatusApi {

    @JsonProperty("subjects")
    private List<Subject> subjects;

    public GetStudentSubjectResponseApi(String code, String message, List<Subject> subjects) {
        super(code, message);
        this.subjects = subjects;
    }
}
