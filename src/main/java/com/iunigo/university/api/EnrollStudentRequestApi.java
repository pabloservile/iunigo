package com.iunigo.university.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

@JsonRootName(value = "enrollStrudentApi")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EnrollStudentRequestApi {

    @JsonProperty("name")
    private String name;

    @JsonProperty("surname")
    private String surname;

    @JsonProperty("subject_name")
    private String subjectName;

    public EnrollStudentRequestApi(String name, String surname, String subjectName) {
        this.name = name;
        this.surname = surname;
        this.subjectName = subjectName;
    }
}
