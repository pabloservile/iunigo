package com.iunigo.university.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.iunigo.university.app.dto.StatusStudent;
import lombok.*;

import java.util.List;

@JsonRootName(value = "studentStatusResponseApi")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StudentStatusResponseApi extends StatusApi{

    private List<StatusStudent> statusStudentList;

}
