package com.iunigo.university.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

import java.time.LocalDate;

@JsonRootName(value = "createMarkRequestApi")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CreateMarkRequestApi {

    @JsonProperty("dni")
    private String dni;
    @JsonProperty("date")
    private LocalDate date;
    @JsonProperty("subject_id")
    private Long subjectId;
    @JsonProperty("note")
    private Integer note;

    public CreateMarkRequestApi(String dni, LocalDate date, Long subjectId, Integer note) {
        this.dni = dni;
        this.date = date;
        this.subjectId = subjectId;
        this.note = note;
    }
}
