package com.iunigo.university.app.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.iunigo.university.domain.enums.SubjectStatus;
import com.iunigo.university.domain.utils.Pair;
import lombok.*;

import java.util.List;

@JsonRootName(value = "statusStudent")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StatusStudent {

    private String nameAndSurname;
    private List<Pair<String, SubjectStatus>> status;

    public StatusStudent(String nameAndSurname, List<Pair<String, SubjectStatus>> status) {
        this.nameAndSurname = nameAndSurname;
        this.status = status;
    }
}
