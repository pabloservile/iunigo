package com.iunigo.university.domain.service;

import com.iunigo.university.domain.model.Mark;

import java.time.LocalDate;

public interface MarkService {
    Mark save(String dni, LocalDate date, Long subjectId, Integer note);
}
