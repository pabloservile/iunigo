package com.iunigo.university.domain.service;

import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.Subject;

import java.util.List;

public interface StudentSubjectService {

    List<Subject> findSubjectsByStudent(Student student);

}
