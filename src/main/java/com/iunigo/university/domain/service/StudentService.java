package com.iunigo.university.domain.service;

import com.iunigo.university.api.StudentStatusResponseApi;
import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;

import java.util.NoSuchElementException;

public interface StudentService {

    StudentSubject enrollStudent(String name, String surname, String dni, String subjectName) throws NoSuchElementException;
    Student findByDni(String dni) throws NoSuchElementException;
    StudentStatusResponseApi getStudentsStatus();

}
