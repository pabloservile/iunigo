package com.iunigo.university.domain.service.impl;

import com.iunigo.university.domain.model.Mark;
import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;
import com.iunigo.university.domain.model.Subject;
import com.iunigo.university.domain.repository.MarkRepository;
import com.iunigo.university.domain.repository.StudentRepository;
import com.iunigo.university.domain.repository.StudentSubjectRepository;
import com.iunigo.university.domain.repository.SubjectRepository;
import com.iunigo.university.domain.service.MarkService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@Service
public class MarkServiceImpl implements MarkService {

    private Log log = LogFactory.getLog(MarkServiceImpl.class);
    private final String logPattern = "[ Mark Service ] ";


    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private StudentSubjectRepository studentSubjectRepository;

    @Autowired
    private MarkRepository markRepository;


    @Override
    public Mark save(String dni, LocalDate date, Long subjectId, Integer note) {
        log.info(logPattern + "init save mark");

        Subject subject = subjectRepository.findById(subjectId).orElseThrow(NoSuchElementException::new);
        Student student = studentRepository.findByDni(dni).orElseThrow(NoSuchElementException::new);
        StudentSubject studentSubject = studentSubjectRepository.findByStudentAndSubject(student, subject).orElseThrow(NoSuchElementException::new);

        Mark mark = new Mark(studentSubject, note, date);

        log.info(logPattern + "saving mark");
        return markRepository.save(mark);
    }

}
