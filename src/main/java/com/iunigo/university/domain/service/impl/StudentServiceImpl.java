package com.iunigo.university.domain.service.impl;

import com.iunigo.university.api.StudentStatusResponseApi;
import com.iunigo.university.app.dto.StatusStudent;
import com.iunigo.university.domain.enums.SubjectStatus;
import com.iunigo.university.domain.model.Mark;
import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;
import com.iunigo.university.domain.model.Subject;
import com.iunigo.university.domain.repository.MarkRepository;
import com.iunigo.university.domain.repository.StudentRepository;
import com.iunigo.university.domain.repository.StudentSubjectRepository;
import com.iunigo.university.domain.repository.SubjectRepository;
import com.iunigo.university.domain.service.StudentService;
import com.iunigo.university.domain.utils.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.OptionalDouble;

@Service
public class StudentServiceImpl implements StudentService {

    Log log = LogFactory.getLog(StudentServiceImpl.class);
    private final String logPattern = "[ StudentServiceImpl      ] ";

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private StudentSubjectRepository studentSubjectRepository;

    @Autowired
    private MarkRepository markRepository;

    @Override
    public StudentSubject enrollStudent(String name, String surname, String dni, String subjectName) throws NoSuchElementException {
        log.info(logPattern + "enroll student init");
        Student student = studentRepository.findByNameAndSurnameAndDni(name, surname, dni).orElseThrow(NoSuchElementException::new);
        Subject subject = subjectRepository.findBySubjectName(subjectName).orElseThrow(NoSuchElementException::new);
        StudentSubject studentSubject = new StudentSubject(student, subject);
        return studentSubjectRepository.save(studentSubject);
    }

    @Override
    public Student findByDni(String dni) throws NoSuchElementException {
        log.info(logPattern + "findById init");
        return studentRepository.findByDni(dni).orElseThrow(() -> new NoSuchElementException("Can't find student with dni: " + dni));

    }

    @Override
    public StudentStatusResponseApi getStudentsStatus() {
        log.info(logPattern + "get student status init");
        List<StatusStudent> statusStudentList = new ArrayList<>();

        studentRepository.findAll().forEach(student -> {
            List<StudentSubject> studentSubjectList = studentSubjectRepository.findByStudent(student);
            List<Pair<String, SubjectStatus>> statusList = new ArrayList<>();

            studentSubjectList.stream().filter(listElement -> listElement != null).forEach(subject -> {
                List<Mark> markList = markRepository.findByStudentSubject(subject);
                if (!markList.isEmpty()) {
                    OptionalDouble average = markList.stream().mapToDouble(a -> a.getNote()).average();
                    boolean lessThanSeven = markList.stream().anyMatch(n -> n.getNote() < 7);
                    SubjectStatus ss = (average.getAsDouble() >= 7 && !lessThanSeven) ? SubjectStatus.APROBO : SubjectStatus.NO_APROBO;
                    Pair<String, SubjectStatus> status = new Pair(subject.getSubject().getSubjectName(), ss);
                    statusList.add(status);
                }
            });

            StatusStudent statusStudent = new StatusStudent();
            statusStudent.setNameAndSurname(student.getName() + " " + student.getSurname());
            statusStudent.setStatus(statusList);
            statusStudentList.add(statusStudent);
        });
        log.info(logPattern + "completed status student list");

        StudentStatusResponseApi studentStatusResponseApi = new StudentStatusResponseApi();
        studentStatusResponseApi.setStatusStudentList(statusStudentList);

        return studentStatusResponseApi;
    }

}
