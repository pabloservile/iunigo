package com.iunigo.university.domain.service.impl;

import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;
import com.iunigo.university.domain.model.Subject;
import com.iunigo.university.domain.repository.StudentRepository;
import com.iunigo.university.domain.repository.StudentSubjectRepository;
import com.iunigo.university.domain.service.StudentSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StudentSubjectServiceImpl implements StudentSubjectService {

    @Autowired
    private StudentSubjectRepository studentSubjectRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Subject> findSubjectsByStudent(Student student) {

        List<StudentSubject> subjects = studentSubjectRepository.findByStudent(student);
        List<Subject> subjectList = new ArrayList<>();
        subjects.parallelStream().forEach(s -> subjectList.add(s.getSubject()));
        return subjectList;

    }
}
