package com.iunigo.university.domain.utils;

public class Pair<L,R> {

  private final L subject;
  private final R status;

  public Pair(L subject, R status) {
    assert subject != null;
    assert status != null;

    this.subject = subject;
    this.status = status;
  }

  public L getSubject() { return subject; }
  public R getStatus() { return status; }

  @Override
  public int hashCode() { return subject.hashCode() ^ status.hashCode(); }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Pair)) return false;
    Pair pairo = (Pair) o;
    return this.subject.equals(pairo.getSubject()) &&
           this.status.equals(pairo.getStatus());
  }

}