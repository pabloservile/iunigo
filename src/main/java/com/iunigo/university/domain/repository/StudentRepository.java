package com.iunigo.university.domain.repository;

import com.iunigo.university.domain.model.Student;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "students", path = "students")
public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {

    Optional<Student> findById(@Param("id") Long id);
    Optional<Student> findByDni(@Param("dni") String dni);
    Optional<Student> findByNameAndSurnameAndDni(@Param("name") String name,
                                                 @Param("surname") String surname,
                                                 @Param("dni") String dni);

}
