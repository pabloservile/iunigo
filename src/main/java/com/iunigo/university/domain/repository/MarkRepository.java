package com.iunigo.university.domain.repository;

import com.iunigo.university.domain.model.Mark;
import com.iunigo.university.domain.model.StudentSubject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "marks", path = "marks")
public interface MarkRepository extends PagingAndSortingRepository<Mark, Long> {

    Optional<Mark> findById(@Param("id") Long id);
    Mark save(Mark mark);
    List<Mark> findByStudentSubject(StudentSubject studentSubject);

}
