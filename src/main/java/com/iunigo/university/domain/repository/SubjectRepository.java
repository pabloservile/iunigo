package com.iunigo.university.domain.repository;

import com.iunigo.university.domain.model.Subject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "subjects", path = "subjects")
public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long> {

    Optional<Subject> findById(@Param("id") Long id);
    Optional<Subject> findBySubjectName(@Param("subjectName") String subjectName);

}

