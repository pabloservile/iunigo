package com.iunigo.university.domain.repository;

import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;
import com.iunigo.university.domain.model.Subject;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "studentsubjects", path = "studentsubjects")
public interface StudentSubjectRepository extends PagingAndSortingRepository<StudentSubject, Long> {

    StudentSubject save(StudentSubject studentSubject);
    List<StudentSubject> findByStudent(Student student);
    Optional<StudentSubject> findByStudentAndSubject(Student student, Subject subject);

}
