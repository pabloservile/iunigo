package com.iunigo.university.domain.enums;

public enum SubjectStatus {

    APROBO, NO_APROBO;
}
