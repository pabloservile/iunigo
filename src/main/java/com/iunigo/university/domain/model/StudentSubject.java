package com.iunigo.university.domain.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"student_id", "subject_id"})
})
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StudentSubject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long studentSubjectId;

    @ManyToOne(fetch=FetchType.EAGER)
    private Student student;

    @ManyToOne(fetch=FetchType.EAGER)
    private Subject subject;

    @OneToMany(mappedBy = "studentSubject", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Mark> marks;

    public StudentSubject(Student student, Subject subject) {
        this.student = student;
        this.subject = subject;
    }

}
