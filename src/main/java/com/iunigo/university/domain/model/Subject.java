package com.iunigo.university.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String subjectName;
    @OneToMany(
            mappedBy="subject",
            cascade=CascadeType.ALL
            , fetch = FetchType.LAZY
    )
    @JsonIgnore
    @ToString.Exclude
    private List<StudentSubject> studentSubjectList;

    public Subject(String subjectName) {
        this.subjectName = subjectName;
    }
}
