package com.iunigo.university.domain.model;

import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Mark {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;
    @ManyToOne(fetch=FetchType.LAZY)
    private StudentSubject studentSubject;
    @Range(min = 0, max = 10)
    private Integer note;
    private LocalDate testDate;

    public Mark(StudentSubject studentSubject, Integer note, LocalDate testDate) {
        this.studentSubject = studentSubject;
        this.note = note;
        this.testDate = testDate;
    }
}
