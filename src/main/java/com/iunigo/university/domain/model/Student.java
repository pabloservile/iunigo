package com.iunigo.university.domain.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long studentId;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "surname", nullable = false, length = 100)
    private String surname;

    @Column(name = "dni", nullable = false, length = 50, unique = true)
    private String dni;

    @OneToMany(
        mappedBy="student",
        cascade=CascadeType.ALL, fetch = FetchType.LAZY
    )
    @ToString.Exclude
    private List<StudentSubject> studentSubject;

    public Student(String name, String surname, String dni) {
        this.name = name;
        this.surname = surname;
        this.dni = dni;
    }

}
