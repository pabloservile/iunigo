package com.iunigo.university.domain.rest;

import com.iunigo.university.api.EnrollStudentRequestApi;
import com.iunigo.university.api.GetStudentSubjectResponseApi;
import com.iunigo.university.api.StatusApi;
import com.iunigo.university.api.StudentStatusResponseApi;
import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.Subject;
import com.iunigo.university.domain.service.StudentService;
import com.iunigo.university.domain.service.StudentSubjectService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/students")
@CrossOrigin
public class StudentController {

    private Log log = LogFactory.getLog(StudentController.class);
    private final String logPattern = "[ EnrollStudentController ] ";

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentSubjectService studentSubjectService;

    @PostMapping("/{dni}/subject")
    public ResponseEntity<StatusApi> enrollStudent(
            @PathVariable("dni") String dni,
            @RequestBody EnrollStudentRequestApi enrollStudentRequestApi) {
        log.info(logPattern + "init enroll student controller");
        StatusApi response = null;
        try {
            String name = enrollStudentRequestApi.getName();
            String surname = enrollStudentRequestApi.getSurname();
            String subjectName = enrollStudentRequestApi.getSubjectName();

            studentService.enrollStudent(name, surname, dni, subjectName);

            log.info(logPattern + "student enrolled, returning response");
            response = new StatusApi("201", "Student enrolled succesfully to subject");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            response = new StatusApi("409", "Can't enroll student to subject");
            log.info(logPattern + "conflict enrolling student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        } catch (DataIntegrityViolationException e) {
            response = new StatusApi("409", "Can't enroll student more than one time in same subject");
            log.info(logPattern + "conflict enrolling student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        } catch (Exception e) {
            response = new StatusApi("500", "Can't enroll student to subject - generic error");
            log.info(logPattern + "conflict enrolling student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @GetMapping("/{dni}/subjects")
    public ResponseEntity<?> getStudentSubjects(
            @PathVariable("dni") String dni) {
        log.info(logPattern + "init get student subjects controller");
        StatusApi response = null;
        try {
            Student student = studentService.findByDni(dni);
            List<Subject> subjectList = studentSubjectService.findSubjectsByStudent(student);

            log.info(logPattern + "got student subjects");
            response = new GetStudentSubjectResponseApi("200", "got student subjects", subjectList);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            response = new StatusApi("409", "Can't get student subjects");
            log.info(logPattern + "conflict getting subjects from student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        }catch (Exception e) {
            response = new StatusApi("500", "Can't get student subjects - generic error");
            log.info(logPattern + "conflict enrolling student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/status")
    public ResponseEntity<?> getStudentsStatus(){
        log.info(logPattern + "init get student status controller");
        StatusApi response = null;
        try {
            StudentStatusResponseApi responseDto = studentService.getStudentsStatus();
            log.info(logPattern + "got students status");

            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            response = new StatusApi("409", "Can't get student subjects");
            log.info(logPattern + "conflict getting subjects from student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        }catch (Exception e) {
            response = new StatusApi("500", "Can't get student subjects - generic error");
            log.info(logPattern + "conflict enrolling student, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
