package com.iunigo.university.domain.rest;

import com.iunigo.university.api.CreateMarkRequestApi;
import com.iunigo.university.api.StatusApi;
import com.iunigo.university.domain.service.MarkService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Constraint;
import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/marks")
@CrossOrigin
public class MarkController {

    private Log log = LogFactory.getLog(MarkController.class);
    private final String logPattern = "[ MarkController ] ";

    @Autowired
    private MarkService markService;


    @PostMapping()
    public ResponseEntity<StatusApi> createMark(
            @RequestBody CreateMarkRequestApi createMarkRequestApi) {
        log.info(logPattern + "create mark controller init");
        StatusApi response = null;
        try {
            String dni = createMarkRequestApi.getDni();
            LocalDate date = createMarkRequestApi.getDate();
            Long subjectId = createMarkRequestApi.getSubjectId();
            Integer note = createMarkRequestApi.getNote();
            markService.save(dni, date, subjectId, note);

            log.info(logPattern + "mark created, returning response");
            response = new StatusApi("201", "mark created succesfully");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            response = new StatusApi("409", "can't create mark");
            log.info(logPattern + "conflict creating mark, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        } catch (ConstraintViolationException e) {
            response = new StatusApi("409", "can't create mark,violation in size note");
            log.info(logPattern + "conflict creating mark, violation in size note, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.CONFLICT);
        }catch (Exception e) {
            response = new StatusApi("500", "Can't create mark - generic error");
            log.info(logPattern + "internal server error creating mark, returning response");

            return new ResponseEntity<StatusApi>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
