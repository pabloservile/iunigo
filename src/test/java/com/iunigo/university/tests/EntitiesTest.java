package com.iunigo.university.tests;

import com.iunigo.university.domain.model.Mark;

import static org.assertj.core.api.Assertions.assertThat;

import com.iunigo.university.domain.model.Student;
import com.iunigo.university.domain.model.StudentSubject;
import com.iunigo.university.domain.model.Subject;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@SpringBootTest
public class EntitiesTest {

    private Mark mark;
    private Subject subject;
    private Student student;
    private StudentSubject studentSubject;

    @BeforeEach
    public void setUpEntities() {
        this.mark = new Mark();
        this.mark.setNote(5);
        this.mark.setTestDate(LocalDate.now());

        this.subject = new Subject();
        this.subject.setSubjectName("Matematica");

        this.student = new Student();
        this.student.setName("Pablo");
        this.student.setDni("5674736");
        this.student.setSurname("Perez");

        this.studentSubject = new StudentSubject();
        this.studentSubject.setSubject(this.subject);
        this.studentSubject.setStudent(this.student);
    }

    @Test
    public void testEntities() {
        // Testing mark
        assertThat(this.mark.getNote()).isEqualTo(5);
        assertThat(this.mark.getTestDate()).isEqualTo(LocalDate.now());

        // Testing subject
        assertThat(this.subject.getSubjectName()).isEqualTo("Matematica");

        // Testing student
        assertThat(this.student.getName()).isEqualTo("Pablo");
        assertThat(this.student.getSurname()).isEqualTo("Perez");
        assertThat(this.student.getDni()).isEqualTo("5674736");

        // Testing studentSubject
        assertThat(this.studentSubject.getStudent()).isEqualTo(this.student);
        assertThat(this.studentSubject.getSubject()).isEqualTo(this.subject);
    }

}
