# Iunigo: test técnico

## Índice

+ [Introducción]
+ [Motivación]
+ [Intalación]
+ [Api Reference]
+ [Procesos]
+ [Tests]
+ [Properties]

##Introducción
El proyecto es un módulo requerido por iúnigo como instancia técnica del proceso

##Motivación
Se crea en base a la necesidad de lo solicitado según iúnigo extendiendo la funcionalidad requerida por parte del negocio.

##Instalación
Se debe instalar primero el ambiente y luego el proyecto.

### Instalación del ambiente
Se deberán instalar las siguientes aplicaciones:

- [Java 1.8](#nstalacion.ambiente.java8)
- [Maven](#nstalacion.ambiente.maven)
- [Git](#nstalacion.ambiente.git)
- [Eclipse](#nstalacion.ambiente.eclipse)

<a name="instalacion.ambiente.java8"></a>
#### Java 1.8

Instalar el JDK 1.8.

```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer 
```

Definir la variable de entorno *JAVA_HOME* editando el archivo que contiene variables de entorno.

```
sudo nano /etc/environment
```

Al final del archivo, agregar la siguiente línea:

```
JAVA_HOME="/usr/lib/jvm/java-8-oracle"
```

Ejecutar el archivo *enviroment*.

```
source /etc/enviroment
```

Comprobar la variable de entorno *JAVA_HOME*.

```
echo $JAVA_HOME
```

<a name="instalacion.ambiente.maven"></a>
#### Maven

Instalar Maven.

```
sudo apt-get update
sudo apt-get install maven
```

Definir la variable de entorno *M2_HOME* editando el archivo que contiene variables de entorno.

```
sudo nano /etc/environment
```

Al final del archivo, agregar la siguiente línea:

```
M2_HOME="/usr/share/maven"
```

Ejecutar el archivo *enviroment*.

```
source /etc/enviroment
```

Comprobar la variable de entorno *M2_HOME*.

```
echo $M2_HOME
```

<a name="instalacion.ambiente.git"></a>
#### Git

Instalar Git.

```
sudo apt-get update
sudo apt-get install git
```

Configurar usuario y correo del usuario de GitHub.

```
git config --global user.name "nombre_usuario"
git config --global user.email "email"
```

<a name="instalacion.ambiente.eclipse"></a>
### Eclipse

Descargar la aplicación [Eclipse](https://www.eclipse.org/downloads/).

Descomprimir el archivo descargado e instalar el Eclipse

```
tar xfz ~/Downloads/eclipse-inst-linux64.tar.gz ~/Downloads/eclipse-installer/eclipse-inst
```

Se abrirá la ventana del instalador y seleccionar la opción **Eclipse IDE for Java EE Developers**.
Una vez instalada la aplicación, de deberá configurar Maven. 
Ir al menú Windows >>Preferences >> Maven >> Installations
Añadir un Nuevo Maven Runtime con la siguiente configuración:
```
Installation Type: External
Installation home: {ubicación de la aplicación Maven}
Installation name: maven
```
Seleccionar el nuevo runtime para que sea el *default*.


<a name="instalacion.proyecto"></a>
### Instalación del proyecto
#### Descargar el proyecto desde GitHub

Se debe clonar el proyecto localmente en la carpeta destinada a los *proyectos*.

```
git clone https://gitlab.com/pabloservile/iunigo.git
```

El proyecto cuenta principalmente con **un** branch:

1. master

Se creará el directorio /*iunigo* con la versión actualizada del branch *master*.

#### Importar el proyecto en Eclipse
En Eclipse, importar el proyecto.
Ir a File >> Import
Seleccionar Maven >> Existing Maven Projects

Se deberá configurar el *Run Configurations*.
Seleccionar el proyecto e ir a Run >> Run Configurations.
Seleccionar la configuración *Java Application*, añadir una nueva y configurarlo de la siguiente manera.
```
Main Tab
Name: university
Main class: com.iunigo.university.UniversityApplication
Arguments Tab
VM arguments section
-Dspring.profiles.active=local -Dserver.port=8989 -Duser.timezone=UTC
```

Se deberá configurar las propiedades del *Java Application*.
Ir a Project >> Properties.
Seleccionar la propieda de *Java Build Path*.
Configurarlo de la siguiente manera.
```
Source Tab
Source folders on build path:
    > university/src/main/java
    > university/src/main/resources
Allow output folders for source folder checked.
Default output folder: university/target/classes
```

#### Configurar conexión a la base de datos 
No es necesario ya que utilizamos H2, podemos ingresar a la misma mediante el siguiente URL:
```
http://localhost:8989/h2-console
```

##API Reference
El contexto del proyecto para acceder a los servicios es ***/iunigo***. La URL dependerá del ambiente que se necesite consumir los servicios.

Servicios activos de spring boot con información útil:

|Spring Boot|
|---|
|/swagger-ui.html|

|Ambiente|URL|
|---|---|
|Local|http://localhost:8989/iunigo/|

##Procesos

### Servicios disponibles
|URL|Descripción|Métodos|
|---|---|---|
|http://localhost:8989/iunigo/api/marks|Genera una nueva nota.|POST|
|http://localhost:8989/iunigo/api/students/{dni}/subject|Inscribe un alumno en una materia.|POST|
|http://localhost:8989/iunigo/api/students/{dni}/subjects|Consulta las materias que posee un alumno|GET|
|http://localhost:8989/iunigo/api/students/status|Consulta las materias que tiene aprobadas o no el alumno|GET|

## Tests
El proyecto cuenta con Test Unitarios. 

<a name="unitarios"></a>
### Unitarios: 
Los test unitarios estan dentro del folder: src/test/java
En el mismo encontraran un package por cada modelo que se vaya testeando.

La herramienta que se utiliza para estos test es: PowerMock + Mockito, lo que nos brinda la posibilidad de testear el flujo funcional sin tener que utilizar base de datos y conexiones externas.

La convención para los test es:
- El test debe contener el nombre de la clase que se testea con el sufijo "Test", EJ: MarkServiceImpl
- El test debe tener un nombre descriptivo (no importa la longitud) ejemplo: givenAStudentIdIsNotFound_WhenGetStudentByIdMethodIsCalled_ThenNoSuchElementException
- El test debe realizar todas las verificaciones posibles sobre el flujo (Cantidad de invocaciones, datos, excepciones, etc).

Para correr los test ejecutar el comando: mvn test

## Properties
El archivo /src/main/resources/*application.yml* contiene las propiedades generales del proyecto.

### Propiedades generales

|Propiedad|Descripción|
|---|---|
|server.port|Puerto de la aplicación.|
|server.servlet.context-path|Ruta de Contexto de la aplicación.|

### Variables de entorno en application.yml

Ambiente Local

|Nombre|Valor|
|---|---|
|DATABASE_URL|jdbc:h2:mem:testdb|
|DATABASE_USER|sa|
|DATABASE_PASS|{ninguna}|


## Servicios disponibles para probar
La aplicación ha sido deployada en una VPS por lo cual accediendo a la url http://142.93.15.46:8989/iunigo se encuentran los recursos disponibles.


